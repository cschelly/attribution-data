//
//  TEALAttributionConstants.m
//
//  Created by Christina on 12/20/18.
//  Copyright © 2018 Tealium. All rights reserved.
//

#import "TEALAttributionConstants.h"

NSString *const TEALAdvertisingIdentifierKey = @"device_advertising_id";
NSString *const TEALAdvertisingVendorIdentifierKey = @"device_advertising_vendor_id";
NSString *const TEALAdvertisingTrackingEnabledKey = @"device_advertising_enabled";
NSString *const TEALAdUserClickedLast30Days = @"ad_user_clicked_last_30_days";
NSString *const TEALAdUserClickedDate = @"ad_user_date_clicked";
NSString *const TEALAdUserConversionDate = @"ad_user_date_converted";
NSString *const TEALAdOrganizationName = @"ad_org_name";
NSString *const TEALAdCampaignIdentifier = @"ad_campaign_id";
NSString *const TEALAdCampaignName = @"ad_campaign_name";
NSString *const TEALAdGroupIdentifier = @"ad_group_id";
NSString *const TEALAdGroupName = @"ad_group_name";
NSString *const TEALAdKeyword = @"ad_keyword";
