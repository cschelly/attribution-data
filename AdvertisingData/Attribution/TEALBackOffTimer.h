//
//  TEALBackOffTimer.h
//  AdvertisingData
//
//  Created by Christina on 1/4/19.
//  Copyright © 2019 Christina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TEALBackOffTimer : NSTimer

@property (assign) NSTimeInterval timerInterval;
@property (assign) BOOL finished;

+ (TEALBackOffTimer *) backOffTimerWithBlock:(void(^)(TEALBackOffTimer *timer))block;

@end

