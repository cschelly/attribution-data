//
//  TEALAttributionManager.m
//
//  Created by Christina on 12/20/18.
//  Copyright © 2018 Tealium. All rights reserved.
//

#import "TEALAttributionManager.h"
#import <AdSupport/AdSupport.h>
#import <iAd/iAd.h>

@interface TEALAttributionManager ()
@end

@implementation TEALAttributionManager

- (void)enable {
    self.isEnabled = YES;
}

- (void)disable {
    self.isEnabled = NO;
}

- (BOOL)isEnabled {
    return _isEnabled == YES;
}

- (BOOL)isDisabled {
    return _isEnabled == NO;
}

- (void)enableSearchAds {
    self.searchAdsIsEnabled = YES;
}

- (void)disableSearchAds {
    self.searchAdsIsEnabled = NO;
}

- (BOOL)searchAdsIsEnabled {
    return _searchAdsIsEnabled == YES;
}

- (BOOL)searchAdsIsDisabled {
    return _searchAdsIsEnabled == NO;
}

- (NSDictionary *)attributionData {
    if (_isEnabled) {
        self.isAdTrackingAllowed = self.isTrackingAllowed ? @"true" : @"false";
        return @{
                 TEALAdvertisingIdentifierKey : self.idfa,
                 TEALAdvertisingVendorIdentifierKey : self.idfv,
                 TEALAdvertisingTrackingEnabledKey : _isAdTrackingAllowed
                 };
    } else {
        return @{};
    }
}

- (NSDictionary *)appleAttributionData {
    if (_searchAdsIsEnabled) {
        return _appleAttributionDetails;
    } else {
        return @{};
    }
}

- (NSString *)idfa {
    if ([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]) {
        NSUUID *idfa =
        [[ASIdentifierManager sharedManager] advertisingIdentifier];
        return [idfa UUIDString];
    }
    return @"00000000-0000-0000-0000-000000000000";
}

- (NSString *)idfv {
    if ([[[UIDevice currentDevice] identifierForVendor] UUIDString]) {
        NSString *idfv =
        [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        return idfv;
    }
    return @"";
}

- (BOOL)isTrackingAllowed {
    if ([[ASIdentifierManager sharedManager] isAdvertisingTrackingEnabled]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)requestAppleAttributionDetails {
    [[ADClient sharedClient] requestAttributionDetailsWithBlock:^(
                                                                  NSDictionary *attributionDetails,
                                                                  NSError *error) {
        if (error) {
            NSLog(@"Request Search Ads attributes failed with error: %@",
                  error.description);
            if (error.code == ADClientErrorLimitAdTracking) {
                NSLog(@"Limit Ad Tracking is enabled for this device.");
            }
            return;
        } else {
            NSString *objectVersion = @"Version3.1";
            attributionDetails = attributionDetails[objectVersion];
            self->_appleAttributionDetails = @{
                                               TEALAdUserClickedLast30Days :
                                                   attributionDetails[@"iad-attribution"],
                                               TEALAdUserClickedDate : attributionDetails[@"iad-click-date"],
                                               TEALAdUserConversionDate :
                                                   attributionDetails[@"iad-conversion-date"],
                                               TEALAdOrganizationName : attributionDetails[@"iad-org-name"],
                                               TEALAdCampaignIdentifier : attributionDetails[@"iad-campaign-id"],
                                               TEALAdCampaignName : attributionDetails[@"iad-campaign-name"],
                                               TEALAdGroupIdentifier : attributionDetails[@"iad-adgroup-id"],
                                               TEALAdGroupName : attributionDetails[@"iad-adgroup-name"],
                                               TEALAdKeyword : attributionDetails[@"iad-keyword"]
                                               };

        }
    }];
}

@end
