//
//  TEALAttributionManager.h
//
//  Created by Christina on 12/20/18.
//  Copyright © 2018 Tealium. All rights reserved.
//

#import "TEALAttributionConstants.h"
#import <Foundation/Foundation.h>

@interface TEALAttributionManager : NSObject

@property(nonatomic) BOOL isEnabled;
@property(nonatomic) BOOL searchAdsIsEnabled;
@property(nonatomic, strong, readwrite) NSDictionary *appleAttributionDetails;
@property(nonatomic, readwrite) NSString *isAdTrackingAllowed;

/**
 Enables attribution data.
 */
- (void)enable;

/**
 Disables attribution data.
 */
- (void)disable;

/**
 Checks if the attribution data is disabled

 @returns BOOL: YES if the attribution data is disabled
 */
- (BOOL)isDisabled;

/**
 Enables apple search ads data.
 */
- (void)enableSearchAds;

/**
 Disables apple search ads data.
 */
- (void)disableSearchAds;

/**
 Checks if the apple search ads data is disabled

 @returns BOOL: YES if the apple search ads data is disabled
 */
- (BOOL)searchAdsIsDisabled;

/**

 @returns NSDictionary of attribution details if attribution is enabled
 else @returns an empty dictionry
 */
- (NSDictionary *)attributionData;

/**

 @returns NSDictionary of apple attribution details if attribution is enabled
 else @returns an empty dictionry
 */
- (NSDictionary *)appleAttributionData;

/**
 Checks if user allowed IDFA usage

 @returns NSUUID: advertising identifier if advertising tracking is enabled
 if disabled, returns a dummy string of zeroes
 */

- (NSString *)idfa;

/**

 @returns NSUUID: advertising vendor identifier if advertising tracking is
 enabled
 */

- (NSString *)idfv;

/**
 Checks if advertising tracking is enabled

 @returns BOOL: YES if advertising tracking is enabled
 */

- (BOOL)isTrackingAllowed;

/**
 Makes a request to Apple's servers for any applicable Search Ad Attribution
 data.
 */
- (void)requestAppleAttributionDetails;

@end
