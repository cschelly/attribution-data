//
//  TEALAttributionConstants.h
//
//  Created by Christina on 12/20/18.
//  Copyright © 2018 Tealium. All rights reserved.
//

#import <Foundation/Foundation.h>

// attribution data
extern NSString *const TEALAdvertisingIdentifierKey;
extern NSString *const TEALAdvertisingVendorIdentifierKey;
extern NSString *const TEALAdvertisingTrackingEnabledKey;

// apple ads data
// True if user clicked on a Search Ads impression within 30 days prior to app
// download.
extern NSString *const TEALAdUserClickedLast30Days;
// Date and time the user clicked on a corresponding ad
extern NSString *const TEALAdUserClickedDate;
// Date and time the user downloaded your app
extern NSString *const TEALAdUserConversionDate;
// The organization that owns the campaign which the corresponding ad was part
// of.
extern NSString *const TEALAdOrganizationName;
// The ID of the campaign which the corresponding ad was part of.
extern NSString *const TEALAdCampaignIdentifier;
// The name of the campaign which the corresponding ad was part of
extern NSString *const TEALAdCampaignName;
// The ID of the ad group which the corresponding ad was part of
extern NSString *const TEALAdGroupIdentifier;
// The name of the ad group which the corresponding ad was part of.
extern NSString *const TEALAdGroupName;
// The keyword that drove the ad impression which led to the corresponding ad
// click.
extern NSString *const TEALAdKeyword;
