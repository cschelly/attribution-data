//
//  TEALBackOffTimer.m
//  AdvertisingData
//
//  Created by Christina on 1/4/19.
//  Copyright © 2019 Christina. All rights reserved.
//

#import "TEALBackOffTimer.h"

@interface TEALBackOffTimer ()

@property (strong) NSTimer *backOffTimer;
@property (copy) void (^backOffBlock)(TEALBackOffTimer *timer);

@end

@implementation TEALBackOffTimer

+ (TEALBackOffTimer *) backOffTimerWithBlock:(void (^)(TEALBackOffTimer *timer))block {
    TEALBackOffTimer *this = [[TEALBackOffTimer alloc] init];
    this.timerInterval = 0.0f;
    this.backOffTimer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                         target:this
                                                       selector:@selector(executeBackOffInterval:)
                                                       userInfo:nil
                                                        repeats:NO];
    this.backOffBlock = block;
    return this;
}

- (void)executeBackOffInterval:(NSTimer *)timer {
    self.backOffBlock(self);
    if (!self.finished) {
        self.timerInterval = _timerInterval >= 0.2 ? _timerInterval * 2 : 0.2;
        self.timerInterval = MIN(60.0, _timerInterval);
        _backOffTimer = [NSTimer scheduledTimerWithTimeInterval:_timerInterval
                                                         target:self
                                                       selector:@selector(executeBackOffInterval:)
                                                       userInfo:nil
                                                        repeats:NO];
    }
}

@end
