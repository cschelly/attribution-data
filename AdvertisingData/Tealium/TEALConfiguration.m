//
//  TEALConfiguration.m
//  AdvertisingData
//
//  Created by Christina on 12/25/18.
//  Copyright © 2018 Christina. All rights reserved.
//

#import "TEALConfiguration.h"

@interface TEALConfiguration ()
@end

@implementation TEALConfiguration

+ (instancetype)configurationWithAccount:(NSString *)accountName
                                 profile:(NSString *)profileName
                             environment:(NSString *)environmentName {
    TEALConfiguration *configuration = [[TEALConfiguration alloc] init];

    configuration.accountName = [accountName lowercaseString];
    configuration.profileName = [profileName lowercaseString];
    configuration.environmentName = [environmentName lowercaseString];
    configuration.enableAttribution = YES;
    configuration.enableSearchAds = YES;

    return configuration;
}

@end
