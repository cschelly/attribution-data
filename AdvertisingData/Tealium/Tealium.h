//
//  Tealium.h
//  AdvertisingData
//
//  Created by Christina on 12/25/18.
//  Copyright © 2018 Christina. All rights reserved.
//

#import "TEALAttributionManager.h"
#import "TEALConfiguration.h"
#import <Foundation/Foundation.h>

@interface Tealium : NSObject

@property(strong, nonatomic) TEALAttributionManager *attributionManager;

+ (_Nonnull instancetype)newInstanceForKey:(NSString *_Nonnull)key
                             configuration:(TEALConfiguration *_Nonnull)configuration;

- (void)trackEventWithTitle:(NSString *_Nonnull)title
                dataSources:(NSDictionary *_Nullable)customDataSources;

- (void)trackViewWithTitle:(NSString *_Nonnull)title
               dataSources:(NSDictionary *_Nullable)customDataSources;

@end
