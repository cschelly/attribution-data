//
//  Tealium.m
//  AdvertisingData
//
//  Created by Christina on 12/25/18.
//  Copyright © 2018 Christina. All rights reserved.
//

#import "Tealium.h"
#import "TEALBackOffTimer.h"
#define NSLog(FORMAT, ...)\
fprintf(stderr, "%s\n",\
[[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

@interface Tealium ()

@property (strong) TEALBackOffTimer *backOffTimer;

@end

@implementation Tealium

typedef void (^TEALBooleanCompletionBlock)(BOOL success,
                                           NSError *_Nullable error);

+ (instancetype)newInstanceForKey:(NSString *_Nonnull)key
                    configuration:(TEALConfiguration *)configuration {
    return [Tealium
            newInstanceForKey:key
            configuration:configuration
            completion:^(BOOL success, NSError *_Nullable error) {
                if (error) {
                    NSLog(
                          @"Problem initializing Tealium instance: %@ error:%@",
                          key, error);
                }
            }];
}

+ (instancetype)newInstanceForKey:(NSString *_Nonnull)key
                    configuration:(TEALConfiguration *)configuration
                       completion:(TEALBooleanCompletionBlock)completion {
    Tealium *instance = [Tealium instanceWithConfiguration:configuration
                                                completion:completion];
    return instance;
}

+ (instancetype)instanceWithConfiguration:(TEALConfiguration *_Nonnull)configuration
completion:(TEALBooleanCompletionBlock _Nullable)completion {

    Tealium *instance =
    [[Tealium alloc] initPrivateWithConfiguration:configuration];

    __weak Tealium *weakInstance = instance;

    [weakInstance finalizeWithConfiguration:configuration
                                 completion:completion];

    return instance;
}

- (instancetype)initPrivateWithConfiguration:
(TEALConfiguration *_Nonnull)configuration {
    if (self = [super init]) {
        _attributionManager = [[TEALAttributionManager alloc] init];
    }

    return self;
}

- (void)finalizeWithConfiguration:(TEALConfiguration *)configuration
                       completion:(TEALBooleanCompletionBlock)setupCompletion {
    BOOL success = NO;
    NSError *error = nil;

    if (configuration.enableAttribution) {
        [_attributionManager enable];
    }
    if (configuration.enableSearchAds) {
        [_attributionManager enableSearchAds];
        [_attributionManager requestAppleAttributionDetails];
    }

    if (setupCompletion) {
        setupCompletion(success, error);
    }
}

- (void)trackEventWithTitle:(NSString *)title
                dataSources:(NSDictionary *)clientDataSources {
    [self trackType:@"link" title:title dataSources:clientDataSources];
}

- (void)trackViewWithTitle:(NSString *)title
               dataSources:(NSDictionary *)clientDataSources {
    [self trackType:@"view" title:title dataSources:clientDataSources];
}

- (void)trackType:(NSString *)eventType
            title:(NSString *)title
      dataSources:(NSDictionary *)clientDataSources {

    __block NSDictionary *clientData = [clientDataSources copy];
    NSDictionary *attributionData = [_attributionManager attributionData];
    NSMutableDictionary *payload = [NSMutableDictionary dictionary];
    [payload addEntriesFromDictionary:clientData];
    [payload addEntriesFromDictionary:attributionData];
    if ([_attributionManager searchAdsIsEnabled]) {
        self.backOffTimer = [TEALBackOffTimer backOffTimerWithBlock:^(TEALBackOffTimer *timer) {
            if ([self->_attributionManager appleAttributionData]) {
                NSLog(@"%@", [NSString stringWithFormat:@"%f",timer.timerInterval]);
                timer.finished = YES;
                NSDictionary *appleAttributionData = [self->_attributionManager appleAttributionData];
                [payload addEntriesFromDictionary:appleAttributionData];
                NSLog(@"======TRACK CALL======");
                if ([self->_attributionManager isEnabled]) {
                    NSLog(@"======👍ATTRIBUTION AND SEARCH ADS ENABLED👍======");
                } else {
                    NSLog(@"======👎ATTRIBUTION NOT ENABLED BUT SEARCH ADS ENABLED👍======");
                }
                NSLog(@"%@", [NSString stringWithFormat:@"%s%@\r%s",
                              "call_type: ", eventType, ""]);
                NSLog(@"%@", [NSString stringWithFormat:@"%s%@\r%s",
                              "tealium_event: ", title, ""]);
                for (id key in payload)
                    NSLog(@"%@",
                          [NSString stringWithFormat:@"%@%s%@\r%s", key, ": ",
                           [payload objectForKey:key], ""]);
            }
        }];
    } else {
        NSLog(@"======TRACK CALL======");
        if ([self->_attributionManager isEnabled]) {
            NSLog(@"======👍ATTRIBUTION ENABLED BUT SEARCH ADS NOT ENABLED👎======");
        } else {
            NSLog(@"======👎ATTRIBUTION AND SEARCH ADS NOT ENABLED👎======");
        }
        NSLog(@"%@", [NSString stringWithFormat:@"%s%@\r%s",
                      "call_type: ", eventType, ""]);
        NSLog(@"%@", [NSString stringWithFormat:@"%s%@\r%s",
                      "tealium_event: ", title, ""]);
        for (id key in payload)
            NSLog(@"%@",
                  [NSString stringWithFormat:@"%@%s%@\r%s", key, ": ",
                   [payload objectForKey:key], ""]);
    }

}

@end
