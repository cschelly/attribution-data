//
//  TEALConfiguration.h
//  AdvertisingData
//
//  Created by Christina on 12/25/18.
//  Copyright © 2018 Christina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TEALConfiguration : NSObject

@property(nonatomic, copy) NSString *_Nonnull accountName;
@property(nonatomic, copy) NSString *_Nonnull profileName;
@property(nonatomic, copy) NSString *_Nonnull environmentName;
@property(nonatomic) BOOL enableAttribution;
@property(nonatomic) BOOL enableSearchAds;

+ (instancetype _Nonnull)
configurationWithAccount:(NSString *_Nonnull)accountName
profile:(NSString *_Nonnull)profileName
environment:(NSString *_Nonnull)environmentName;

@end
