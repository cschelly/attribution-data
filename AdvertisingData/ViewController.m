//
//  ViewController.m
//  AdvertisingData
//
//  Created by Christina on 12/25/18.
//  Copyright © 2018 Christina. All rights reserved.
//

#import "TEALAttributionConstants.h"
#import "ViewController.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _configuration =
    [TEALConfiguration configurationWithAccount:@"attribution-mgr-tests"
                                        profile:@"unit-tests-profile"
                                    environment:@"unit-tests-env"];
    _tealium = [Tealium newInstanceForKey:@"1" configuration:_configuration];
    // Add delay to give time for apple attr details to be returned
    // (will not need in prod) Might need to save this data to
    // persistentDataSources instead?
    //[NSThread sleepForTimeInterval:1.0];
    _data = @{@"view_key" : @"view_value", @"test_key" : @"test_value"};
    [_tealium trackViewWithTitle:@"main_view" dataSources:_data];
}

- (IBAction)onButtonTapped:(id)sender {
    _data = @{@"link_key" : @"link_value", @"test_key" : @"test_value"};
    [_tealium trackEventWithTitle:@"button_click" dataSources:_data];
}

@end
