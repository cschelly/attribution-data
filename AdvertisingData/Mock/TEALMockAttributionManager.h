//
//  TEALMockAttributionManager.h
//  AdvertisingData
//
//  Created by Christina on 12/26/18.
//  Copyright © 2018 Christina. All rights reserved.
//

#import "TEALAttributionConstants.h"
#import <Foundation/Foundation.h>

@interface TEALMockAttributionManager : NSObject

@property(nonatomic) BOOL isEnabled;
@property(nonatomic) BOOL searchAdsIsEnabled;
@property(nonatomic, strong, readwrite) NSDictionary *appleAttributionDetails;

/**
 Enables attribution manager.
 */
- (void)enable;

/**
 Disables attribution manager.
 */
- (void)disable;

/**
 Checks if the attribution manager is enabled

 @returns BOOL: YES if the attribution manager is enabled
 */
- (BOOL)isEnabled;

/**
 Enables apple search ads data.
 */
- (void)enableSearchAds;

/**
 Disables apple search ads data.
 */
- (void)disableSearchAds;

/**

 @returns NSDictionary of apple attribution details if attribution is enabled
 else @returns an empty dictionry
 */
- (NSDictionary *)appleAttributionData;

/**
 Makes a request to Apple's servers for any applicable Search Ad Attribution
 data.
 */
- (void)requestAppleAttributionDetails;

@end
