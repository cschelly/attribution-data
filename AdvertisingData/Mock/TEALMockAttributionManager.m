//
//  TEALMockAttributionManager.m
//  AdvertisingData
//
//  Created by Christina on 12/26/18.
//  Copyright © 2018 Christina. All rights reserved.
//

#import "TEALMockAttributionManager.h"

@implementation TEALMockAttributionManager

- (void)enable {
    self.isEnabled = YES;
}

- (void)disable {
    self.isEnabled = NO;
}

- (BOOL)isEnabled {
    return _isEnabled == YES;
}

- (void)enableSearchAds {
    self.searchAdsIsEnabled = YES;
}

- (void)disableSearchAds {
    self.searchAdsIsEnabled = NO;
}

- (BOOL)searchAdsIsEnabled {
    return _searchAdsIsEnabled == YES;
}

- (NSDictionary *)appleAttributionData {
    return _appleAttributionDetails;
}

- (void)requestAppleAttributionDetails {

    NSDictionary *attributionDetails = @{
                                         @"Version3.1" : @{
                                                 @"iad-attribution" : @true,
                                                 @"iad-org-name" : @"Light Right",
                                                 @"iad-campaign-id" : @15292426,
                                                 @"iad-campaign-name" : @"Light Bright Launch",
                                                 @"iad-conversion-date" : @"2016-10-14T17:18:07Z",
                                                 @"iad-click-date" : @"2016-10-14T17:17:00Z",
                                                 @"iad-adgroup-id" : @15307675,
                                                 @"iad-adgroup-name" : @"LightRight Launch Group",
                                                 @"iad-keyword" : @"light right",
                                                 }
                                         };

    NSString *objectVersion = @"Version3.1";
    attributionDetails = attributionDetails[objectVersion];
    self.appleAttributionDetails = @{
                                     TEALAdUserClickedLast30Days : attributionDetails[@"iad-attribution"],
                                     TEALAdUserClickedDate : attributionDetails[@"iad-click-date"],
                                     TEALAdUserConversionDate : attributionDetails[@"iad-conversion-date"],
                                     TEALAdOrganizationName : attributionDetails[@"iad-org-name"],
                                     TEALAdCampaignIdentifier : attributionDetails[@"iad-campaign-id"],
                                     TEALAdCampaignName : attributionDetails[@"iad-campaign-name"],
                                     TEALAdGroupIdentifier : attributionDetails[@"iad-adgroup-id"],
                                     TEALAdGroupName : attributionDetails[@"iad-adgroup-name"],
                                     TEALAdKeyword : attributionDetails[@"iad-keyword"]
                                     };
}
@end
