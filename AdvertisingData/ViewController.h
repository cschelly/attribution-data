//
//  ViewController.h
//  AdvertisingData
//
//  Created by Christina on 12/25/18.
//  Copyright © 2018 Christina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Tealium.h"
#import "TEALAttributionManager.h"

@interface ViewController : UIViewController

@property (nonatomic, strong) Tealium *tealium;
@property (nonatomic, strong) TEALConfiguration *configuration;
@property (nonatomic, copy, readwrite) NSDictionary *data;
@property (nonatomic, strong) TEALAttributionManager *attributionManager;

@end

