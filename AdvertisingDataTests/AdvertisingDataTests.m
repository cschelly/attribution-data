//
//  AdvertisingDataTests.m
//  AdvertisingDataTests
//
//  Created by Christina on 12/25/18.
//  Copyright © 2018 Christina. All rights reserved.
//

#import "TEALAttributionManager.h"
#import "TEALConfiguration.h"
#import "TEALMockAttributionManager.h"
#import "Tealium.h"
#import <XCTest/XCTest.h>

@interface TEALAttributionManagerTests : XCTestCase

@property(nonatomic, strong) Tealium *tealium;
@property(nonatomic, strong) TEALConfiguration *configuration;
@property(nonatomic, strong) TEALAttributionManager *attributionManager;
@property(nonatomic, strong) TEALMockAttributionManager *mockAttributionManager;

@end

@implementation TEALAttributionManagerTests

- (void)setUp {
    [super setUp];
    self.configuration =
    [TEALConfiguration configurationWithAccount:@"attribution-mgr-tests"
                                        profile:@"unit-tests-profile"
                                    environment:@"unit-tests-env"];
    self.configuration.enableAttribution = YES;
    _attributionManager = [[TEALAttributionManager alloc] init];
    _mockAttributionManager = [[TEALMockAttributionManager alloc] init]; // for mock apple data
    [_attributionManager enable];
    [_mockAttributionManager enable];
}

- (void)tearDown {
    self.configuration = nil;
    [super tearDown];
}

#pragma mark - TEALAttributionManager Tests

- (void)testAttributionEnabled {
    XCTAssertEqual([_attributionManager isEnabled], YES);
}

- (void)testAttributionDisabled {
    [_attributionManager disable];
    XCTAssertEqual([_attributionManager isDisabled], YES);
}

- (void)testIsTrackingAllowedTrue {
    BOOL advertisingStatus = YES;
    BOOL advertiserTrackingStatus = [_attributionManager isTrackingAllowed];
    XCTAssertEqual(advertisingStatus, advertiserTrackingStatus);
}

- (void)testIsTrackingAllowedFalse {
    BOOL advertisingStatus = NO;
    BOOL advertiserTrackingStatus = [_attributionManager isTrackingAllowed];
    XCTAssertNotEqual(advertisingStatus, advertiserTrackingStatus);
}

- (void)testAdvertisingIdentifier {
    NSString *idfa = [_attributionManager idfa];
    XCTAssertNotNil(idfa);
}

- (void)testAdvertisingVendorIdentifier {
    NSString *idfv = [_attributionManager idfv];
    XCTAssertNotNil(idfv);
}

- (void)testAttributionDataLengthOfDictionaryWhenEnabled {
    NSUInteger expectedLength = 3;
    NSUInteger actualLength = [[_attributionManager attributionData] count];
    XCTAssertEqual(expectedLength, actualLength);
}

- (void)testAttributionDataLengthOfDictionaryWhenDisabled {
    [_attributionManager disable];
    NSUInteger expectedLength = 0;
    NSUInteger actualLength = [[_attributionManager attributionData] count];
    XCTAssertEqual(expectedLength, actualLength);
}

- (void)testAttributionDataContent {
    NSDictionary *actualAttributionData = [_attributionManager attributionData];
    XCTAssertTrue([[actualAttributionData allKeys] containsObject:@"device_advertising_id"]);
    XCTAssertTrue([[actualAttributionData allKeys] containsObject:@"device_advertising_vendor_id"]);
    XCTAssertTrue([[actualAttributionData allKeys] containsObject:@"device_advertising_enabled"]);
}

- (void)testRequestAppleAttributionDetails {
    [_mockAttributionManager requestAppleAttributionDetails];
    NSDictionary *actualAppleAttributionData = [_mockAttributionManager appleAttributionDetails];
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_user_clicked_last_30_days"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_user_date_clicked"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_user_date_converted"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_org_name"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_campaign_id"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_campaign_name"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_group_id"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_group_name"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_keyword"]);
}

- (void)testAppleAttributionDataLengthOfDictionaryWhenEnabled {
    [_mockAttributionManager requestAppleAttributionDetails];
    NSUInteger expectedLength = 9;
    NSUInteger actualLength =
    [[_mockAttributionManager appleAttributionData] count];
    XCTAssertEqual(expectedLength, actualLength);
}

- (void)testGetAppleAttributionDataLengthOfDictionaryWhenDisabled {
    [_mockAttributionManager disable];
    NSUInteger expectedLength = 0;
    NSUInteger actualLength = [[_mockAttributionManager appleAttributionData] count];
    XCTAssertEqual(expectedLength, actualLength);
}

- (void)testAppleAttributionDataContent {
    [_mockAttributionManager requestAppleAttributionDetails];
    NSDictionary *actualAppleAttributionData = [_mockAttributionManager appleAttributionData];
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_user_clicked_last_30_days"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_user_date_clicked"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_user_date_converted"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_org_name"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_campaign_id"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_campaign_name"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_group_id"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_group_name"]);
    XCTAssertTrue([[actualAppleAttributionData allKeys] containsObject:@"ad_keyword"]);
}

@end
